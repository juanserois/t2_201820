package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.DoublyLinkedList;

public class DoublyLinkedListTest {

	private DoublyLinkedList<Integer> list;

	public void setupEscenario0() {
		list = new DoublyLinkedList<Integer>();
	}

	@Test
	public void testNewLinkedList() {
		setupEscenario0();
		assertTrue(list.getSize() == 0);
		list.add(1);
		assertFalse(list.getSize() == 0);
	}
	
	@Test
	public void testGetElement() {
		setupEscenario0();
		list.add(1);
		assertTrue(list.getFirst().getElement() == 1);
	}
	

}
