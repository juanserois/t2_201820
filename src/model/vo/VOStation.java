package model.vo;

public class VOStation {
	private int id;

	private String name;

	private String city;

	private double latitude;

	private double longitude;

	private double dpCapacity;

	private String online_data;

	public VOStation(int id, String name, String city, double latitude, double longitude, double dpCapacity,
			String online_data) {
		super();
		this.id = id;
		this.name = name;
		this.city = city;
		this.latitude = latitude;
		this.longitude = longitude;
		this.dpCapacity = dpCapacity;
		this.online_data = online_data;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getCity() {
		return city;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public double getDpCapacity() {
		return dpCapacity;
	}

	public String getOnline_data() {
		return online_data;
	}
}
