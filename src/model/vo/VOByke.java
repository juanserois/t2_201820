package model.vo;

/**
 * Representation of a byke object
 */
public class VOByke {
	
	private int bike_id;
	
	
	public VOByke(int id){
		bike_id = id;
	}

	/**
	 * @return id_bike - Bike_id
	 */
	public int id() {
		return bike_id;
	}	
}
