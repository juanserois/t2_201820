package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	private int trip_id;

	private String start_time;

	private String end_time;

	private int bikeid;

	private double tripduration;

	private int from_station_id;

	private String from_station_name;

	private int to_station_id;

	private String to_station_name;

	private String usertype;

	private String gender;

	private String birthyear;

	public VOTrip(int trip_id, String start_time, String end_time, int bikeid, double tripduration, int from_station_id,
			String from_station_name, int to_station_id, String to_station_name, String usertype, String gender,
			String birthyear) {
		super();
		this.trip_id = trip_id;
		this.start_time = start_time;
		this.end_time = end_time;
		this.bikeid = bikeid;
		this.tripduration = tripduration;
		this.from_station_id = from_station_id;
		this.from_station_name = from_station_name;
		this.to_station_id = to_station_id;
		this.to_station_name = to_station_name;
		this.usertype = usertype;
		this.gender = gender;
		this.birthyear = birthyear;
	}

	public int getTrip_id() {
		return trip_id;
	}

	public String getStart_time() {
		return start_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	public int getBikeid() {
		return bikeid;
	}

	public double getTripduration() {
		return tripduration;
	}

	public int getFrom_station_id() {
		return from_station_id;
	}

	public String getFrom_station_name() {
		return from_station_name;
	}

	public int getTo_station_id() {
		return to_station_id;
	}

	public String getTo_station_name() {
		return to_station_name;
	}

	public String getUsertype() {
		return usertype;
	}

	public String getGender() {
		return gender;
	}

	public String getBirthyear() {
		return birthyear;
	}

}
