package model.logic;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IDoublyLinkedList;

public class DivvyTripsManager implements IDivvyTripsManager {

	DoublyLinkedList<VOStation> stations = new DoublyLinkedList<VOStation>();
	DoublyLinkedList<VOTrip> trips = new DoublyLinkedList<VOTrip>();

	public void loadStations(String stationsFile) {
		Reader reader;
		try {
			reader = Files.newBufferedReader(Paths.get(stationsFile));
			CSVReader csvReader = new CSVReader(reader);
			String[] nextRecord;
			csvReader.readNext();
			while ((nextRecord = csvReader.readNext()) != null) {
				VOStation station = new VOStation(Integer.parseInt(nextRecord[0]), nextRecord[1], nextRecord[2],
						Double.parseDouble(nextRecord[3]), Double.parseDouble(nextRecord[4]),
						Double.parseDouble(nextRecord[5]), nextRecord[6]);
				stations.addAtEnd(station);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void loadTrips(String tripsFile) {
		Reader reader;
		try {
			reader = Files.newBufferedReader(Paths.get(tripsFile));
			CSVReader csvReader = new CSVReader(reader);
			String[] nextRecord;
			csvReader.readNext();
			while ((nextRecord = csvReader.readNext()) != null) {
				VOTrip trip = new VOTrip(Integer.parseInt(nextRecord[0]), nextRecord[1], nextRecord[2],
						Integer.parseInt(nextRecord[3]), Double.parseDouble(nextRecord[4]),
						Integer.parseInt(nextRecord[5]), nextRecord[6], Integer.parseInt(nextRecord[7]), nextRecord[8],
						nextRecord[9], nextRecord[10], nextRecord[11]);
				trips.addAtEnd(trip);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public IDoublyLinkedList<VOTrip> getTripsOfGender(String gender) {
		DoublyLinkedList<VOTrip> tripsOfGender = new DoublyLinkedList<VOTrip>();
		trips.setCurrent(trips.getFirst());
		while (trips.getCurrent().getNext() != null) {
			if (trips.getCurrentElement().getGender().equals(gender)) {

				tripsOfGender.add(trips.getCurrentElement());
			}
			trips.currentNext();
		}
		return tripsOfGender;
	}

	@Override
	public IDoublyLinkedList<VOTrip> getTripsToStation(int stationID) {
		DoublyLinkedList<VOTrip> tripsToStation = new DoublyLinkedList<VOTrip>();
		trips.setCurrent(trips.getFirst());
		while (trips.getCurrent().getNext() != null) {
			if (trips.getCurrentElement().getTo_station_id() == (stationID)) {

				tripsToStation.add(trips.getCurrentElement());
			}
			trips.currentNext();
		}
		return tripsToStation;
	}

}
