package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoublyLinkedList<E> extends Iterable<E> {
	

	
	public void add(E pElement);
	
	public void addAtEnd(E pElement);
	
	public void addAtK(E pElement, int k);
	
	public E getElement (int k);
	
	public E getCurrentElement();
	
	public int getSize();
	
	public void delete();
	
	public void deleteAtK(int k);
	
	public void currentNext();
	
	public void prev();
	
	
}
