package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<E> implements IDoublyLinkedList<E> {

	private Node<E> first;
	private Node<E> last;
	private Node<E> current;
	private int size;

	public DoublyLinkedList() {
		size = 0;
		first = null;
		last = null;
		current = null;
	}

	public Node<E> getFirst() {
		return first;
	}

	public Node<E> getLast() {
		return last;
	}

	public Node<E> getCurrent() {
		return current;
	}

	public void setCurrent(Node pCurrent) {
		current = pCurrent;
	}

	@Override
	public Iterator<E> iterator() {
		
		return new Iterator<E>(){

			@Override
			public boolean hasNext() {
				return current != last;
			}

			@Override
			public E next() {
				if (current == null){
					current = first;
					return current.getElement();
				}
				if(current.getNext() == null){
					throw new NoSuchElementException();
				}
				currentNext();
				return current.getElement();
			}
		};
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public E getCurrentElement() {
		return (E) current.getElement();
	}

	@Override
	public void add(E pElement) {
		Node temp = new Node<E>(pElement, first, null);
		if (size == 0) {
			first = last = temp;
		} else {
			first.setPrev(temp);
			first = temp;
		}
		size++;

	}

	@Override
	public void addAtEnd(E pElement) {
		Node temp = new Node<E>(pElement, null, last);
		if (size == 0) {
			first = last = temp;
		} else {
			last.setNext(temp);
			last = temp;
		}
		size++;

	}

	@Override
	public void addAtK(E pElement, int k) {
		int contador = 0;
		current = first;
		while (current.getNext() != null && contador < k) {
			currentNext();
		}
		Node temp = new Node<E>(pElement, current, current.getPrev());
		current.setPrev(temp);
		current.getPrev().setNext(temp);
	}

	@Override
	public E getElement(int k) {
		int contador = 0;
		current = first;
		while (current.getNext() != null && contador < k) {
			currentNext();
		}
		return (E) current.getElement();
	}

	@Override
	public void delete() {
		first = first.getNext();
		first.setPrev(null);
	}

	@Override
	public void deleteAtK(int k) {
		int contador = 0;
		current = first;
		while (current.getNext() != null && contador < k) {
			currentNext();
		}
		current.getPrev().setNext(current.getNext());
		current.getNext().setPrev(current.getPrev());
	}

	@Override
	public void currentNext() {
		current = current.getNext();
	}

	@Override
	public void prev() {
		current = current.getPrev();
	}

}
