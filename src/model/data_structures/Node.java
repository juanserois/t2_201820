package model.data_structures;

public class Node<E> {

	E element;
	Node <E>next;
	Node <E>prev;

	public Node(E pElement, Node pNext, Node pPrev) {
		element = pElement;
		next = pNext;
		prev = prev;
	}
	
	public E getElement() {
		return element;
	}

	public Node getPrev() {
		return prev;
	}
	public Node getNext() {
		return next;
	}
	
	public void setPrev(Node pPrev) {
		prev = pPrev;
	}
	
	public void setNext(Node pNext) {
		next = pNext;
	}
}
